# Converter from PocketGis text format to TomTom's ov2 #

Based on openspeedcam exports:
```
https://openspeedcam.net/export
need registration
```

PocketGis format:
```
IDX,X,Y,TYPE,SPEED,DirType,Direction,Distance,Angle // OpenSpeedcam 26-09-2023 05:47
4,60.651221,56.805247,1,80,1,24,500,25 // 5a1ec7ef113e8c04bd2284c7 | Статическая камера | Кордон
5,60.651473,56.805182,1,80,1,221,311,20 // 5a1ec827113e8c04bd2284c8 | Статическая камера | Кордон | В спину
8,60.634961,56.797482,3,60,1,121,350,15 // 5a1eea3a113e8c04bd2284cb | Контроль проезда на красный | Автоураган
13,34.424651,45.72851,100,60,1,3,500,15 // 5a1ef8ca113e8c04bd2284d1 | Начало населенного пункта |  Джанкой
.....
```
TomTom ov2 format:
```
Each entry in a simple custom OV2 file is as follows.

1 byte: type (always 2)
4 bytes: length of this record in bytes (including the T and L fields)
4 bytes: longitude coordinate of the POI
4 bytes: latitude coordinate of the POI
13 bytes: null−terminated ASCII string specifying the name of the POI
```
For each POI name need same file with picture and voice (if needed)
```
TYPE    Описание	                    Description	            Filename
1    Статическая камера	                Static Speed Camera	    statcam_xx
3    Контроль перекрестка	        Red Light Camera	    redlightcam
4    Парная камера (средняя скорость)	Average Speed Camera	    averagecam_xx
5    Мобильная засада	                Mobile Camera	            mobilecam_xx
1    Контроль полосы	                Lane Camera	            buslinecam
2    Стационарный пост ДПС	        Police Station	            police_st
21   Железнодорожный переезд	        Railroad Crossing	    railway
22   Пешеходный переход	                Сrosswalk	            crosswalk
100  Начало населенного пункта	        Settlement	            city
101  Ограничение скорости	        Speed Limit	            fixed_xx
102  Искусственная неровность	        Speed Bump	            speedbump
103  Плохая дорога	                Bad Road	            bad_road
104  Опасный поворот	                Dangerous Turn	            dangerous_turn
105  Опасный перекресток	        Dangerous Crossroad	    crossroads
106  Другая опасность	                Danger	                    warning
107  Обгон запрещен	                Overtaking is Forbidden	    overtake
```

Converter's feathures
```
- Process one or list of files by mask PocketGis*.txt
- Searching for dubles near 10m - check coordinates for each type and merge it into one between of them.
- Splitting one big file to several smaller. It helps for TomTom for work with it. Max length now is 26000 lines.
- Support operating mode:
        all - searching for dubles for each type of POI (by default only for *cam* and *cross*)
        nopair - no duble searching - all POI coordinates as original
        nomsk - for Russia it may helps to decrease 20% of POI and make you TomTom faster
        debug - keep all csv file after process
```

