# Input File names:
# PocketGis*.txt
# keys: 
#	all - searching pair in all *.csv
#	nomsk - Delete Moscow cameras
#	nopair - no searching pairs - keep all POI as original
#		by default searching pair in *cam* and *cross*
#	debug - without removing working files
##########################################################
#	version 6.0 Russia, September 2023

import os, datetime, fnmatch, re, sys, math
import pip
try:
	from tqdm import tqdm
except ImportError:
	pip.main(['install', 'tqdm'])
	sys.exit()

dict={
'1':'statcam',
'3':'redlightcam',
'4':'averagecam',
'5':'mobilecam',
'11':'buslinecam',
'20':'police_st',
'21':'railway',
'22':'crosswalk',
'100':'city',
'101':'fixed',
'102':'speedbump',
'103':'bad_road',
'104':'dangerous_turn',
'105':'crossroads',
'106':'warning',
'107':'overtake'
}

cameras = list()
max_file_len = 26000 #limit POI for each file
maxDiff = 0.0006 # about 10m between same POI
d = datetime.datetime.now()
p = 'OpenSpeedcam_%s.%s.%s' % (d.day, d.month, d.year)
keymatch = ['cam','cross'] # by default searching pair only for *cam* and *cross*
debug = False
pair_skip = False
nomsk = False
nomskminX = 36.6
nomskmaxX = 38.5
nomskminY = 55.3
nomskmaxY = 56.4

class CameraInfo:
	def __init__(self, x, y, idx, file):
		self.PointX = x
		self.PointY = y
		self.Name = idx
		self.File = file
		pass

# Convert source file with IDX, X, Y, to separate csv files
def convert_file(p, origin_file):
	if debug:
		print('Converting file', origin_file)
	try:
		cnt_lines = sum(1 for line in open(origin_file,'r'))
		with open(origin_file,'r',errors='ignore') as file_read:
			next(file_read) # skip the first line in original file with description
			for lines in tqdm(file_read, total=cnt_lines, desc=origin_file):
				list=lines.split(',')
				if list[3] in ['1','4','5','101']:
					filename=dict[list[3]]+'_'+list[4]
				else:
					filename=dict[list[3]]
				with open(p+'/'+filename+'.csv','a') as new_file:
					new_file.write(','.join(list[1:3])+','+list[0]+'\n')
	except Exception as err:
		print('Error: ', err)
		sys.exit()

# Read file to cameras list
def read_file(p, csv_file):
	if debug:
		print('Reading file', csv_file)
	try:
		with open(p+'/'+csv_file,'r',errors='ignore') as file_read:
			file_name=csv_file.split('.')
			for line in file_read:
				list=line.split(',')
				cameras.append(CameraInfo(list[0], list[1], list[2].rstrip('\n'),file_name[0]))
	except Exception as err:
		print('Error: ', err)
		sys.exit()

# Remove paired cameras about 10m
def diff_cameras(cameras):
	for_delete = []
	for i in range(len(cameras)-1):
		c1 = cameras[i]
		c2 = cameras[i+1]
		diffX = float(c2.PointX)-float(c1.PointX)
		diffY = float(c2.PointY)-float(c1.PointY)
		if abs(diffX)< maxDiff and abs(diffY) < maxDiff:
			c2.PointX=str(round(float(c1.PointX)+diffX/2, 6))
			c2.PointY=str(round(float(c1.PointY)+diffY/2, 6))
			c2.Name=c2.Name+'_'+c1.Name
			for_delete.append(c1)
	for cd in for_delete:
		cameras.remove(cd)
	del for_delete

# Delete POI in Moscow
def remove_msk(cameras):
	if debug:
		print('Remove Moscow')
	for_remove = []
	for i in range(len(cameras)-1):
		c1 = cameras[i]
		if nomskminX <= float(c1.PointX) <= nomskmaxX and nomskminY <= float(c1.PointY) <= nomskmaxY:
			for_remove.append(c1)
	for cd in for_remove:
		cameras.remove(cd)
	del for_remove[:]
	
# Splitting cameras for max_file_len files
def split_cameras(cameras,csv_file):
	cnt = math.ceil(len(cameras) / max_file_len)
	if debug:
		print('Splitting file', csv_file, 'to', cnt, 'files')
	try:
		for i, enum in enumerate(cameras):
			enum.File = '_'.join([enum.File,str((i % cnt) + 1)])
	except Exception as err:
		print('Error: ', err)
		sys.exit()
		
# Convert to OV2 file
def ov2_conv(cameras, p, csv_file):
	if debug:
		print('Converting to ov2 file', csv_file)
	try:
		if len(cameras) > max_file_len:
			split_cameras(cameras, csv_file)
		for i in cameras:
			lnt = 14+len(i.Name)
			lon = int(float(i.PointX)*100000)
			lat = int(float(i.PointY)*100000)
			summary = (bytes([2])+lnt.to_bytes(4, byteorder = 'little')+lon.to_bytes(4, byteorder = 'little', signed=True)+lat.to_bytes(4, byteorder = 'little', signed=True)+bytes(i.Name,'utf-8')+bytes([0]))
			new_file_name = p+'/'+i.File+'.ov2'
			with open(new_file_name,'ab+') as fres:
				fres.write(summary)
	except Exception as err:
		print('Error: ', err)
		sys.exit()

##############################################################
# MAIN flow
print('<< PocketGis to OV2 converter by Amokk >>')
# Check input keys
if len(sys.argv)>1:
	for i in (sys.argv):
		if i in('all'):
			print('Searching pairs for all POI')
			keymatch = ['.csv']
		if i in ('nopair'):
			print('No pair searching')
			pair_skip = True
		if i in ('debug'):
			print('Debug True')
			debug = True
		if i in ('nomsk'):
			print('Delete Moscow cameras')
			nomsk = True
		if i in ('help'):
			print('PocketGisConv keys:\n  all - searching pairs for all POI\n  nopair - no pair searching, keep POI as original\n  nomsk - delete Moscow cameras\n  debug - without removing processed files\n  if no key - by default - searching pairs for *cam* and *cross* POI only')
			sys.exit()
else:
	print('by default - searching pairs for *cam* and *cross* POI only')
# Begin
if not os.path.isdir(p):
	os.mkdir(p)
print('Converting to CSV')
for origin_file in os.listdir('.'):
	if fnmatch.fnmatch(origin_file,'PocketGis*.txt'):
		convert_file(p, origin_file)
print('Converting to OV2')
for csv_file in tqdm(os.listdir(p), total=len(os.listdir(p))):
	read_file(p, csv_file)
	if pair_skip:
		print('No pair searching')
	else:
		for patterns in keymatch:
			if csv_file.__contains__(patterns):
				if nomsk:
					remove_msk(cameras)
				diff_cameras(cameras)
				cameras.sort(key = lambda x: (float(x.PointY)))
				diff_cameras(cameras)
				cameras.sort(key = lambda x: (float(x.PointX)))
				diff_cameras(cameras)
	ov2_conv(cameras, p, csv_file)
	del cameras[:]
if debug:
	print('Keep all csv files')
else:
	for csvfiles in os.listdir(p):
		if fnmatch.fnmatch(csvfiles, '*.csv'):
			os.remove(p+'/'+csvfiles)
d2 = datetime.datetime.now()
td = d2 - d
print('Converted to directory', p, 'finished in',td.seconds, 'sec')
print('<< THE END >>')
